#!/usr/bin/env sh

# get all successfully completed pages jobs in this project
# which still have artifacts attached (artifacts length 1 is the job
# trace left over when the rest have been deleted)
sorted_jobs=$(curl -sS --request GET \
  --header "Authorization: Bearer $BOT_ACCESS_TOKEN" \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  --url "$CI_API_V4_URL/projects/$CI_PROJECT_ID/jobs?scope=success" |
  jq -r 'sort_by(.finished_at) | .[] | select(.name == "pages") | select(.artifacts | length > 1) | .id')

echo "$sorted_jobs" >jobs.json

# ignore the newest iteration
old_jobs=$(head -n -1 jobs.json)

# nothing found, nothing to clean
if [ -z "$old_jobs" ]; then
  echo "Nothing to clean."
  exit 0
fi

# for each job gathered, except for the newest
for job_id in $old_jobs; do

  # delete its artifacts
  curl -sS --request DELETE \
    --header "Authorization: Bearer $BOT_ACCESS_TOKEN" \
    -H 'Content-Type: application/json' \
    -H 'Accept: application/json' \
    --url "$CI_API_V4_URL/projects/$CI_PROJECT_ID/jobs/$job_id/artifacts"

  echo "Deleted artifacts of job id: $job_id."
done
