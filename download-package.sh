#!/bin/sh -e

id="$1"
name="$2"

echo "Downloading ${name}..."
if (curl -s -S -L -f -o artifacts.zip --header "JOB-TOKEN: $CI_JOB_TOKEN" "https://gitlab.com/api/v4/projects/$id/jobs/artifacts/build/download?job=build"); then
  unzip -o artifacts.zip -d public
  echo ""
else
  (printf >&2 "WARNING: download for %s failed!\n" "$name")
  exit 0 # just skip this repo to allow for others to complete successfully
fi
